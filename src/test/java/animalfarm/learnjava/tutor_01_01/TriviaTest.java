package animalfarm.learnjava.tutor_01_01;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by phil on 2017/7/1.
 */
public class TriviaTest {

    @Test
    void When_Give3_Then_Return4()
    {
        Trivia tenLimit = new Trivia();
        int result = tenLimit.compute(3);
        assertEquals(4, result);
    }

    @Test
    void When_Give9_Then_Return10()
    {
        Trivia tenLimit = new Trivia();
        int result = tenLimit.compute(9);
        assertEquals(10, result);
    }

    @Test
    void When_Give10_Then_Return10()
    {
        Trivia tenLimit = new Trivia();
        int result = tenLimit.compute(10);
        assertEquals(10, result);
    }

    @Test
    void When_Give9_Then_Return10()
    {
        Trivia tenLimit = new Trivia();
        int result = tenLimit.compute(19);
        assertEquals(10, result);
    }
}
